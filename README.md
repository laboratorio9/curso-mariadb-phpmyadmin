<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo-sql.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Lab SQL sobre MariaDB y phpMyAdmin</h3>

  <p align="center">
    Proyecto para dockerizar entorno de práctica para curso de SQL sobre MariaDB con phpMyAdmin.
    <br />
    Probado para ejecuciones dentro de entornos "Play With Docker"
    <br />
    <a href="https://gitlab.com/laboratorio9/curso-mariadb-phpmyadmin"><strong>Leer Documentacion »</strong></a>
    <br />
    <br />
    <a href="https://labs.play-with-docker.com/">Play with Docker</a>
    ·
    <a href="https://www.phpmyadmin.net/">phpMyAdmin</a>
    ·
    <a href="https://mariadb.org/download">MariaDB</a>
  </p>
</div>



## Sobre el laboratorio
Este proyecto fue creado con el objetivo de facilitar la puesta en marcha de un entorno SQL en un espacio gratuito de manera que el estudiante no deba distraerse de lo que realmente quiere aprender: SQL.
Se construyó un docker-compose tomando imágenes base oficiales de dockerhub tanto para MariaDB como para phpmyadmin utilizando una base de datos de ejemplo [Sakila](https://dev.mysql.com/doc/index-other.html) publicada por mysql.
El entorno gratuito sugerido para la ejecucion del Lab es "Play With Docker". Su interfaz es sencilla y no requiere de ningun tipo de configuración o instalación adicional.

De ninguna manera se pretende acotar las habilidades de los estudiantes sino simplificar el proceso de aprendizaje. Si cualquiera de ellos asi lo deseara, podría descargar todas las soluciones Open Source aquí utilizadas e instalar el entorno en el equipo que fuera de su interés.

### Sakila

Es una base de datos de ejemplo que provee MySQL. Para el caso práctico y al tratarse de un producto oficial de MySQL nos soluciona perfectamente el problema de generar una base de datos consistente y compatible con el entorno.

<p align="right">(<a href="#top">back to top</a>)</p>

## Tecnologías utilizadas

* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)
* [MariaDB](https://mariadb.org/download)
* [phpmyadmin](https://mariadb.org/download)
* [Play Whith Docker](https://labs.play-with-docker.com/)

<p align="right">(<a href="#top">back to top</a>)</p>

## Puesta en marcha

Se presentan varios yml divididos por clase.
El curso ha sido pensado en 3 clases de 3 horas.

### Requisitos Previos

El unico requisito es tener una maquina virtual en la nube, que permita la ejecución del entorno.
Para un fin práctico, es suficiente tener una cuenta en la plataforma "Play With Docker" que nos provee sesiones de 3 horas.

### Preparar el entorno en PWD

Una vez realizada la autenticacion en Play With Docker se verá esta pantalla:
<div align="center">
  <a href="#">
      <img src="images/playwithdocker-home.png" alt="Logo" width="500" height="500">
  </a>
</div>

Luego de presionar start nos aparece la siguiente pantalla en la cual tenemos una sesion de 4 horas para trabajar y podemos agregar la cantidad de instancias que fueran necesarias obteniendo un terminal por cada una de ellas.
<div align="center">
  <a href="#e">
      <img src="images/playwithdocker-environment.png" alt="Logo" width="1000" height="500">
  </a>
</div>

### Ejecucion del Servicio
1. Descargar el yml dentro de nuestra instancia

```sh
wget https://gitlab.com/laboratorio9/curso-mariadb-phpmyadmin/-/raw/main/docker-compose-clase1.yml
```

2. UP
El servicio inicia con el siguiente comando.
```sh
docker-compose -f docker-compose-clase1.yml up -d
```

3. Una vez finalizada la ejecución del comando anterior, vamos a ver en la interfaz de PWD que se ha publicado el puerto 8080 como se aprecia en la siguiente imagen..
Al presionar sobre el numero de puerto, nos abre el phpmyadmin, donde podremos ingresar con usuario root y la clave "notSecureChangeMe"
La URL generada por PWD es la que se debe compartir con los estudiantes.
<div align="center">
  <a href="#e">
      <img src="images/playwithdocker-published8080.png" alt="Logo" width="800" height="200">
  </a>
</div>

4. DOWN

```sh
docker-compose -f docker-compose-clase1.yml down
```



## Licencia

### GNU GPL v3
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)    
`[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)`
